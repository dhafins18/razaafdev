from django.test import TestCase
from django.test import LiveServerTestCase
from django.test import Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from .views import *
from .models import *
from .forms import *

# Create your tests here.
class UnitTest(TestCase):

    def test_url_index_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_profile_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

    def test_url_subscribe_form_exist(self):
        response = Client().get('/subscribe_form')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    def test_view_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_view_profile(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_view_subscribe_form(self):
        found = resolve('/subscribe_form')
        self.assertEqual(found.func, subscribe_form)

    def test_model_status_create(self):
        record = Status.objects.create(post='Dummy Post')

        count_records = Status.objects.all().count()
        self.assertEqual(count_records, 1)

    def test_model_subscribers_create(self):
        record = Subscribers.objects.create(name='Test', email='Test@test.com', password='Test123')

        count_records = Subscribers.objects.all().count()
        self.assertEqual(count_records, 1)

    def test_form_status_html(self):
        form = Status_Form()
        self.assertIn('class="materialize-textarea', form.as_p())
        self.assertIn('id="id_post', form.as_p())

    def test_form_subscribe_html(self):
        form = Subscribe_Form()
        self.assertIn('class="validate', form.as_p())
        self.assertIn('id="id_name', form.as_p())
        self.assertIn('id="id_email', form.as_p())
        self.assertIn('id="id_password', form.as_p())

    def test_form_status_validation_blank(self):
        form = Status_Form(data={'post': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['post'],
            ["This field is required."]
        )

    def test_form_subscribe_validation_blank(self):
        form = Subscribe_Form(data={'name': '', 'email': '', 'password': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['email'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['password'],
            ["This field is required."]
        )

    def test_post_success_and_render_the_result(self):
        response_post = Client().post('/create_status', {'post': 'Dummy Post'})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('Dummy Post', html_response)

    def test_subscribe_success(self):
        response_post = Client().post('/subscribe', {'name': 'Test', 'email': 'Test@test.com', 'password': 'Test123'})
        self.assertEqual(response_post.status_code, 200)

    def test_validate_email_success(self):
        response_post = Client().get('/validate_email', {'email': 'Test@test.com'})
        self.assertEqual(response_post.status_code, 200)

    def test_post_error_and_render_the_result(self):
        response_post = Client().post('/create_status', {'post': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Dummy Post', html_response)

    def test_url_books_api(self):
        response = Client().get('/get_books_api/quilting')
        self.assertEqual(response.status_code, 200)

    def test_view_books_api(self):
        found = resolve('/get_books_api/quilting')
        self.assertEqual(found.func, get_books_api)

    def test_json_data(self):
        response = Client().get('/get_books_api/quilting').json()
        data = response['kind']
        self.assertIn('books#volumes', data)

    def test_view_validate_api(self):
        found = resolve('/validate_email')
        self.assertEqual(found.func, validate_email)

    def test_view_subscribe(self):
        found = resolve('/subscribe')
        self.assertEqual(found.func, subscribe)

# class FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest,self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(FunctionalTest,self).tearDown()
#
#     def test_judul(self):
#         exp = "Hello, Apa Kabar?"
#
#         selenium = self.selenium
#         # selenium.get('http://razaafdev.herokuapp.com/')
#         selenium.get('http://localhost:8000/')
#         selenium.find_element_by_id("status_card").click()
#         judul = selenium.find_element_by_id('judul').text
#         self.assertIn(judul, exp)
#
#     def test_btn_profile(self):
#         exp = "Share your feelings!"
#
#         selenium = self.selenium
#         # selenium.get('http://razaafdev.herokuapp.com/')
#         selenium.get('http://localhost:8000/')
#         selenium.find_element_by_id("status_card").click()
#         label = selenium.find_element_by_id('share').text
#         self.assertIn(label, exp)
#
#     def test_h5_css(self):
#         font_size = "22.96px"
#
#         selenium = self.selenium
#         # selenium.get('http://razaafdev.herokuapp.com/')
#         selenium.get('http://localhost:8000/')
#         selenium.find_element_by_id("status_card").click()
#         h5_css = selenium.find_element_by_css_selector('h5')
#         h5_font = h5_css.value_of_css_property('font-size')
#
#         self.assertEqual(font_size, h5_font)
#
#     def test_body_css(self):
#         display = "block"
#
#         selenium = self.selenium
#         # selenium.get('http://razaafdev.herokuapp.com/')
#         selenium.get('http://localhost:8000/')
#
#         body_css = selenium.find_element_by_css_selector('body')
#         body_dis = body_css.value_of_css_property('display')
#
#         self.assertEqual(display, body_dis)
#
#     def test_functional_status(self):
#         status_input = "Coba Coba"
#
#         selenium = self.selenium
#         # selenium.get('http://razaafdev.herokuapp.com/')
#         selenium.get('http://localhost:8000/')
#         time.sleep(3)
#         selenium.find_element_by_id("status_card").click()
#         time.sleep(3)
#         selenium.find_element_by_id("id_show").click()
#         time.sleep(3)
#         status_form = selenium.find_element_by_id('id_post')
#         submit_button = selenium.find_element_by_id('id_btn')
#         status_form.send_keys(status_input)
#         submit_button.send_keys(Keys.RETURN)
#
#         self.assertIn(status_input, selenium.page_source)
#
#     def test_change_theme(self):
#         selenium = self.selenium
#         # selenium.get('http://razaafdev.herokuapp.com/profile')
#         selenium.get('http://localhost:8000/profile')
#         time.sleep(3)
#
#         intro_card2_css = selenium.find_element_by_css_selector('.intro')
#         intro_card2_bgc_old = intro_card2_css.value_of_css_property('background-color')
#         intro_card2_fcl_old = intro_card2_css.value_of_css_property('color')
#
#         intro_section_css = selenium.find_element_by_css_selector('.intro-section')
#         intro_section_bgi_old = intro_section_css.value_of_css_property('background-image')
#
#         body_css = selenium.find_element_by_css_selector('body')
#         body_bcg_old = body_css.value_of_css_property('background-color')
#
#         selenium.find_element_by_id('dropdownMenu2').click()
#         selenium.find_element_by_id('changeDark').click()
#
#         intro_card2_bgc_new = intro_card2_css.value_of_css_property('background-color')
#         intro_card2_fcl_new = intro_card2_css.value_of_css_property('color')
#         intro_section_bgi_new = intro_section_css.value_of_css_property('background-image')
#         body_bcg_new = body_css.value_of_css_property('background-color')
#
#         self.assertNotEqual(intro_card2_bgc_old, intro_card2_bgc_new)
#         self.assertNotEqual(intro_card2_fcl_old, intro_card2_fcl_new)
#         self.assertNotEqual(intro_section_bgi_old, intro_section_bgi_new)
#         self.assertNotEqual(body_bcg_old, body_bcg_new)
#
#     def test_accordion(self):
#         selenium = self.selenium
#         # selenium.get('http://razaafdev.herokuapp.com/profile')
#         selenium.get('http://localhost:8000/profile')
#         time.sleep(3)
#
#         inner_panel_css = selenium.find_element_by_id('activities')
#         inner_panel_dis_old = inner_panel_css.value_of_css_property('display')
#
#         selenium.find_element_by_id('act').click()
#
#         inner_panel_dis_new = inner_panel_css.value_of_css_property('display')
#
#         self.assertNotEqual(inner_panel_dis_old, inner_panel_dis_new)
#
#     # def test_loader(self):
#     #     selenium = self.selenium
#     #     # selenium.get('http://razaafdev.herokuapp.com/profile')
#     #     selenium.get('http://localhost:8000/profile')
#     #     loader = selenium.find_element_by_css_selector('.loader')
#     #     content = selenium.find_element_by_css_selector('.content')
#     #
#     #     loader_css_old = loader.value_of_css_property('display')
#     #     content_css_old = content.value_of_css_property('display')
#     #
#     #     time.sleep(3)
#     #
#     #     loader_css_new = loader.value_of_css_property('display')
#     #     content_css_new = content.value_of_css_property('display')
#     #
#     #     self.assertNotEqual(loader_css_old, loader_css_new)
#     #     self.assertNotEqual(content_css_old, content_css_new)
