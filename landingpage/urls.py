from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
	path('', index, name='index'),
	path('profile', profile, name='profile'),
	path('logout', out, name='logout'),
	path('subscribe_form', subscribe_form, name='subscribe_form'),
	path('create_status', create_status, name='create_status'),
	path('subscribe', subscribe, name='subscribe'),
	path('validate_email', validate_email, name='validate_email'),
	path('unsubscribe', unsubscribe, name='unsubscribe'),
	path('subscribers_list', subscribers_list, name='subscribers_list'),
	path('favorite', favorite, name='favorite'),
	path('unfavorite', unfavorite, name='unfavorite'),
	path('get_books_api/<change>', get_books_api, name='get_books_api'),
]
