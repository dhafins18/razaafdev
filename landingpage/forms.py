from django import forms

class Status_Form(forms.Form):
    post = forms.CharField(
        required = True,
        max_length = 300,
        widget = forms.Textarea(
        attrs = {
        'placeholder' : 'bagaimana kesehatan mentalmu hari ini?',
        'class' : 'materialize-textarea',
        'data-length' : 300}))

class Subscribe_Form(forms.Form):
    name = forms.CharField(
        required = True,
        max_length = 100,
        widget = forms.TextInput(
            attrs = {'class' : 'validate', 'name' : 'Name'}
        )
    )

    email = forms.EmailField(
        required = True,
        max_length = 100,
        widget = forms.EmailInput(
            attrs = {'class' : 'validate', 'name' : 'Email'}
        )
    )

    password = forms.CharField(
        required = True,
        max_length = 100,
        widget = forms.PasswordInput(
            attrs = {'class' : 'validate', 'name' : 'Password'}
        )
    )
