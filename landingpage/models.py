from django.db import models

class Status(models.Model):
    post = models.CharField(max_length=300)
    tanggal = models.DateTimeField(auto_now_add=True, blank=True)

class Subscribers(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    password = models.CharField(max_length=255)

    def as_dict(self):
        return {
            "name": self.name,
            "email": self.email,
            "password": self.password
        }
