# Razaaf Dev

developed by : Razaqa Dhafin Haffiyan
1706039484 - Computer Science, Universitas Indonesia

# Pipelines

[![pipeline status](https://gitlab.com/dhafins18/razaafdev/badges/master/pipeline.svg)](https://gitlab.com/dhafins18/razaafdev/commits/master)

# Coverage

[![coverage report](https://gitlab.com/dhafins18/razaafdev/badges/master/coverage.svg)](https://gitlab.com/dhafins18/razaafdev/commits/master)
