from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import Status_Form

# Create your tests here.
class UnitTest(TestCase):

    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    def test_view_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_create(self):
        # Creating a new status
        record = Status.objects.create(post='Dummy Post')

        # Retrieving all available status
        count_records = Status.objects.all().count()
        self.assertEqual(count_records, 1)

    def test_form_input_html(self):
        form = Status_Form()
        self.assertIn('class="validate', form.as_p())
        self.assertIn('id="id_post', form.as_p())

    def test_form_validation_blank(self):
        form = Status_Form(data={'post': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['post'],
            ["Please fill out this field."]
        )

    def test_post_success(self):
        response_post = Client().post('/create_status/', {'post': 'Dummy Post'})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('Dummy Post', html_response)

    def test_post_error(self):
        response_post = Client().post('/create_status/', {'post': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('Dummy Post', html_response)
