$(document).ready(function(){
    $('ul.tabs').tabs();
});

$('#name').after('<i class="material-icons prefix">account_circle</i>');
$('#email').after('<i class="material-icons prefix">email</i>');
$('#password').after('<i class="material-icons prefix">verified_user</i>');

var email_available = false;

var addSubscriber = function(name, email, password) {
    $.ajax({
        type: "POST",
        url: "/subscribe",
        data: {
            name: name,
            email: email,
            password: password,
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function() {
            alert('berhasil disimpan');
            $('#id_name').val('');
            $('#id_email').val('');
            $('#id_password').val('');
        },
        error: function (error) {
            alert(error.responseText);
        }
    });
};

var validateEmail = function (event) {
    event.preventDefault();
    var email = $('#id_email').val();
    $.ajax({
        url: '/validate_email',
        data: {
            email: email
        },
        dataType: 'json',
        success: function(response) {
            if (response.is_taken) {
                $('#helper-text-email').text('email is registered');
                $('#id_email').addClass('red-line');
                email_available = true;
            } else {
                $('#helper-text-email').text('');
                $('#id_email').removeClass('red-line');
                email_available = false;
            }
        }
    });
}

var checkValid = function() {
    var name = $("#id_name").val();
    var email = $("#id_email").val();
    var password = $("#id_password").val();
    if (name != '' && email != '' && password != '' && !email_available) {
        $("#id_subs").prop('disabled', false);
    } else {
        $("#id_subs").prop('disabled', true);
    }
};

$("#form").on("submit", function(event) {
    event.preventDefault();
    var name = $("#id_name").val();
    var email = $("#id_email").val();
    var password = $("#id_password").val();
    addSubscriber(name, email, password);
    getAllData();
});

$("#id_email").on('keyup', function (event) {
    validateEmail(event);
    checkValid();
});


$("#id_name").on('keyup', function (event) {
    checkValid();
});


$("#id_password").on('keyup', function (event) {
    checkValid();
});

$(document).ready(function() {
    getAllData();
});

var delData = function(i, email, password) {
    console.log('aaaaa');
    var input = prompt("Please type your password", "");
    console.log(password);
    console.log(input);
    if (input != password) {
        alert("password not match");
    } else {
        $.ajax({
            type: 'POST',
            url: '/unsubscribe',
            data: {
                email: email,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            dataType: 'json',
            success: function (response) {
                alert(response.msg);
                getAllData();
            }
        });
    }
}

var getAllData = function() {
    $('#subscribers_list_table').empty();
    $.ajax({
        type: 'GET',
        url: '/subscribers_list',
        data: {
            get_param: 'value'
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            var html = '';
            for (var i = 0; i < data.subs.length; i++) {
                html += '<tr><td>' +
                data.subs[i].name + '</td><td>' +
                data.subs[i].email + '</td><td>' +
                '<btn id="btn-sub-' + i + '" onclick="delData(' + i + ', `' + data.subs[i].email + '`, `' + data.subs[i].password + '`)" class="waves-effect waves-light btn">Unsubscribe</btn></td></tr>';
            }
            $('#subscribers_list_table').append(html);
        }
    });
}
