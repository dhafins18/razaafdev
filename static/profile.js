$('document').ready(function(e) {
    setTimeout(function() {
        $("head").append('<style type="text/css"></style>');
        var newStyleElement = $("head").children(':last');
        newStyleElement.html(".content{display: block;}");

        $("head").append('<style type="text/css"></style>');
        var newStyleElement = $("head").children(':last');
        newStyleElement.html(".loader {height: 0;width: 0;margin: 0;display: none;}");
    }, 2000);
});

$('.toggle').click(function(e) {
  	e.preventDefault();

    var $this = $(this);

    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
    } else {
        $this.parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().find('li .inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
    }
});

$('#changeDark').click(function(e) {

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html("body{font-size: 16px;font-family: 'Poppins', serif;width: 100%;height: 100%;margin: 0;color: #333;background-color: black;}");

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html(".intro{padding: 0 20px;margin-top: 100px;text-align: center;color: white;background-color: black;}");

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html(".intro-section:after{content: '';position: absolute;bottom: 0;left: 0;right: 0;height: 150px;z-index: -1;background-color: black;}");

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html(".intro-section{padding-top: 200px;position: relative;background-size: cover;z-index: 1;background-image: url('https://wallpapercave.com/wp/KGCvZgn.jpg');}");
});

$('#changeLight').click(function(e) {

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html("body{font-size: 16px;font-family: 'Poppins', serif;width: 100%;height: 100%;margin: 0;color: #333;background-color: white;}");

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html(".intro{padding: 0 20px;margin-top: 100px;text-align: center;color: black;background-color: white;}");

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html(".intro-section:after{content: '';position: absolute;bottom: 0;left: 0;right: 0;height: 150px;z-index: -1;background-color: white;}");

    $("head").append('<style type="text/css"></style>');
    var newStyleElement = $("head").children(':last');
    newStyleElement.html(".intro-section{padding-top: 200px;position: relative;background-size: cover;z-index: 1;background-image: url('https://images.adsttc.com/media/images/5017/aeef/28ba/0d44/3100/0c6d/slideshow/stringio.jpg?1414573411');}");
});
