$('.collapsible').collapsible();

$(document).ready(function() {
    change_quilting();
});

function fav(x) {
    $(x).toggleClass("far");
    $(x).toggleClass("fas");
    if ($(x).hasClass("far")) {
        // var old = parseInt($('#counter_fav').text());
        // $('#counter_fav').text(old - 1);
        $.ajax({
            type: "GET",
            url: "/unfavorite",
            success: function(data) {
                $('#counter_fav').text(data);
            }
        });
    } else {
        // var old = parseInt($('#counter_fav').text());
        // $('#counter_fav').text(old + 1);
        $.ajax({
            type: "GET",
            url: "/favorite",
            success: function(data) {
                $('#counter_fav').text(data);
            }
        });
    }
}

function change_quilting() {
    var change_text = $('#change_quilting').val();
    var html = '';
    $('#books').empty();
    if (html == '') {
        $.ajax({
            type: 'GET',
            url: '/get_books_api/' + change_text,
            data: { get_param: 'value' },
            dataType: 'json',
            success: function (data) {
                for (var i = 0; i < data.items.length; i++) {

                    html += '<tr><td>' +
                    '<i onclick="fav(this)" class="far fa-star"></i></td><td>' +
                     data.items[i].volumeInfo.title + '</td><td>' +
                     data.items[i].volumeInfo.authors + '</td><td>' +
                     '<img src=' + data.items[i].volumeInfo.imageLinks.thumbnail + '></td></tr>'
                }
                $('#books').append(html);
            }
        });
    }
}
